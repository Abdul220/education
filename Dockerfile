FROM ubuntu
MAINTAINER Renat Druzhko <Renat_Druzhk@epam.com>
RUN apt update && \
    apt install -y openjdk-8-jre-headless && \
    apt install -y openjdk-8-jdk-headless && \
    apt install -y gnupg2 && \
    apt install -y wget  && \
    wget -q -O - https://pkg.jenkins.io/debian/jenkins.io.key | apt-key add - && \
    sh -c 'echo deb http://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list' && \
    apt update && \
    apt install -y maven && \
    apt install -y jenkins
EXPOSE 8080
VOLUME ["/var/lib/jenkins"]
CMD service jenkins start && bash
